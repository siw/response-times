#!/usr/bin/python3.7
# pip3 install matplotlib==2.2.2
import csv
from collections import defaultdict

import matplotlib.pyplot as plt


PERCENTILES = (50, 95, 99, 100)  # tuple of which percentiles we care about


def calc_percentile(N, P):
    n = max(int(round(P * 0.01 * len(N) + 0.5)), 2)
    return sorted(N)[n - 2]


def get_data():
    data = defaultdict(list)
    # converted .txt to csv using `$ sed -i 's/ /,/g'`
    f = csv.DictReader(open("webservicedata.txt"))
    for row in f:
        # converted to milliseconds
        data[row["concurrency"]].append(int(row["timemicroseconds"]) / 1000)
    return data


def plot():
    coords = {key: {"x": [], "y": []} for key in PERCENTILES}
    for percentile in PERCENTILES:
        for concurrency, response_times in get_data().items():
            # add plot coords for each percentile line
            coords[percentile]["x"].append(concurrency)
            coords[percentile]["y"].append(calc_percentile(response_times, percentile))

        # plot the line as well as individual coords to make it more readable
        plt.plot(coords[percentile]["x"], coords[percentile]["y"], label=f"p{percentile}")
        plt.plot(coords[percentile]["x"], coords[percentile]["y"], "o")

    plt.xlabel("concurrency")
    plt.ylabel("response time (ms)")
    plt.legend()
    plt.savefig("out.png")


if __name__ == "__main__":
    plot()
