Please see the webservicedata.txt file. It contains 5 columns of data. Each line represents one http call to a webservice, as measured from the client side. The client in this case is a load testing system which makes sets of calls at a given level of concurrency, finishes those calls, then moves onto the next concurrency level. The webservice is an unspecified third party service we wish to use.

Calls have been made with varying levels of concurrency in the client, starting at single threaded all the way up to 37 concurrent client threads calling the webservice. Each line identifies which client thread is making the call.

The fields are:

date : the date
time : the time the request finished
concurrency : the number of concurrent requests currently being made by the client
clientid : the id of the client thread making the particular request (eg when the client has 5 threads making calls, clientid will be 0,1,2,3 or 4)
timemicroseconds : the time the request took to finish, in microseconds

Please investigate the data and tell me what you can about the webservice and its performance chatacteristics that the load testing client is calling. The aim is to maximise throughput - what might we change to achieve that goal (both server and client)?
